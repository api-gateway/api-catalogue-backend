import { readFileSync } from 'fs';
import app from './app';
import { sslCertPath, sslKeyPath } from './utils/environment';
import https from 'https';
import { isProdEnv } from './utils/isProdEnv';

if (isProdEnv()) {
  // Configure SSL options for HTTPS
  const sslOptions = {
    key: readFileSync(sslKeyPath),
    cert: readFileSync(sslCertPath),
  };

  // Create HTTPS server with the SSL configuration
  const httpsServer = https.createServer(sslOptions, app);
  httpsServer.listen(3000, () => {
    console.log('Server started on https://localhost:3000');
  });
} else {
  // Start HTTP server for development
  app.listen(3000, () => {
    console.log('Server started on http://localhost:3000');
  });
}
