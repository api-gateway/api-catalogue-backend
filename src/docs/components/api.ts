const apiComponent = {
  Api: {
    type: 'object',
    properties: {
      id: {
        type: 'string',
        description: 'The UUID of the API in the WSO2 or Consul environment.',
      },
      name: {
        type: 'string',
        description: 'The name of the API.',
      },
      shortDescription: {
        type: 'string',
        description: 'A concise summary of the API, limited to 255 characters.',
        maxLength: 255,
      },
      description: {
        type: 'string',
        description: 'A brief description of the API.',
      },
      context: {
        type: 'string',
        description: 'The context or environment of the API.',
      },
      imageUrl: {
        type: 'string',
        description:
          'The URL of the image that represents the API on the catalogue page.',
      },
      tags: {
        type: 'array',
        items: {
          type: 'string',
        },
        description: 'A list of tags that describe the API.',
      },
      businessOwner: {
        type: 'string',
        description: 'The business owner of the API.',
      },
      technicalOwner: {
        type: 'string',
        description: 'The technical owner of the API.',
      },
      version: {
        type: 'string',
        description: 'The version of the API.',
      },
      provider: {
        type: 'string',
        description: 'The provider of the API.',
      },
      featured: {
        type: 'boolean',
        description:
          'Indicates if the API is featured in the API Catalogue, which means displayed on the main page.',
        default: false,
      },
      tenant: {
        type: 'string',
        description: 'The tenant of the API.',
      },
      openapiDefinition: {
        type: 'object',
        description: 'The OpenAPI definition for the API.',
        additionalProperties: true,
      },
    },
    required: [
      'id',
      'name',
      'shortDescription',
      'description',
      'context',
      'businessOwner',
      'technicalOwner',
      'version',
      'provider',
      'tenant',
      'openapiDefinition',
    ],
  },
};

export default apiComponent;
